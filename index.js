const { Tiger, Monkey, Snake } = require('./classes/animals');
const Jungle = require('./classes/jungle');
const Food = require('./classes/food');

let animals = [];

//Tigers
animals.push(new Tiger());
animals.push(new Tiger());
animals.push(new Tiger());
animals.push(new Tiger());

//Monkeys
animals.push(new Monkey());
animals.push(new Monkey());
animals.push(new Monkey());

//Snakes
animals.push(new Snake());
animals.push(new Snake());

const fish = new Food('Fish');
const meat = new Food('Meat');
const grain = new Food('Grain');
const food = [ fish, meat, grain ];

const jungle = new Jungle(animals, food);

jungle.performActions();
jungle.performActions();
jungle.performActions();
jungle.performActions();
jungle.performSounds();