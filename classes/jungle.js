class Jungle {
    _animals;
    _food;

    constructor(animals = [], food = []) {
        this._animals = animals;
        this._food = food;
    }

    addAnimal(animal) {
        this._animals.push(animal);
    }

    addFood(food) {
        this._food.push(food);
    }

    get food() {
        return this._food;
    }

    get animals() {
        return this._animals;
    }

    performSounds() {
        this._animals.forEach((animal, i) => {
            const className = animal.constructor.name;
            animal.makeSound();
            console.log(`${className}[${i}] energy ${animal.energy}`);
        });
    }

    performActions() {
        this._animals.forEach((animal, i) => {
            const className = animal.constructor.name;
            const actions = this.getAllMethods(animal);
            const action = actions[Math.floor(Math.random() * actions.length)];
            if (action !== 'eat') {
                console.log(`${className}[${i}] ${action}`);
                animal[action]();
            } else {
                const food = this._food[Math.floor(Math.random() * this._food.length)];
                console.log(`${className}[${i}] ${action} ${food.type}`);
                animal.eat(food);
            }
        });
    }

    getAllMethods(obj) {
        let methods = [];

        do {
            const l = Object.getOwnPropertyNames(obj)
                .filter((value, i, arr) =>
                    typeof obj[value] === 'function' &&  //only the methods
                    value !== 'constructor' &&           //not the constructor
                    (i == 0 || value !== arr[i - 1]) &&  //not overriding in this prototype
                    methods.indexOf(value) === -1          //not overridden in a child
                );
            methods = methods.concat(l);
        }
        while (
            (obj = Object.getPrototypeOf(obj)) &&   //walk-up the prototype chain
            Object.getPrototypeOf(obj)              //not the the Object prototype methods (hasOwnProperty, etc...)
        )
    
        return methods;
    }

}
module.exports = Jungle;