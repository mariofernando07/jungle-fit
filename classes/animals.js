class Animal {
    
    constructor() {
        this.energy = 0;
    }

    eat(food) {
        this.energy += 5;
    }

    sleep() {
        this.energy += 10;
    }

    makeSound() {
        this.energy -=3;
    }
}

class Monkey extends Animal {
  
    eat(food) {
        this.energy += 2;
    }

    makeSound() {
        this.energy -=4;
    }

    play() {
        this.energy -=8;
        let msg = this.energy > 0 ? '\tOooo Ooo' : '\tI’m too tired';
        console.log(msg);
    }

}

class Tiger extends Animal {
  
    sleep() {
        this.energy += 5;
    }

    eat(food) {
        if(food.type !== 'Grain') {
            this.energy += 5;
        } else {
            console.log(`\tI can’t it that`);

        }
    }

}

class Snake extends Animal { }

module.exports = { Monkey, Tiger, Snake };